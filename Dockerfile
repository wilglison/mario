FROM midianet/serve:19.0.0

COPY audio /opt/audio
COPY img /opt/img
COPY css /opt/css
COPY js  /opt/js
COPY index.html /opt
COPY package.json /opt

WORKDIR /opt

ENTRYPOINT serve

EXPOSE 3000

